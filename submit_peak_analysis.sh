#!/bin/bash

# John Blischak
# 09 Oct 2012

# Usage:
# qsub -l h_vmem=32g -N analyze_peaks -wd /mnt/lustre/home/jdblischak/tag/sequences/JUNB/18358_H/MACS_sub_0.01_10,30/

R --vanilla < *_model.r
R --vanilla < ~/tag/analysis/analyze_peaks.R
python ~/tag/analysis/FRiP.py ../yy1_merged.sub.9400091.bam ../MACS_sub_.0001/MACS_sub_.0001_peaks.bed


