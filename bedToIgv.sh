#!/bin/bash

# John Blischak
# 16 Oct 2012
# Makes a batch script to be run with the IGV from the Broad. Makes .png
# images of the top 10 and bottom 10 peaks (p < .01).
################################################################################

# Sort by the -10log10(pval) in column 5 in reverse order s.t. the most
# significant peaks are at the top of the file. If the -10log10(pval) is the
# same number, it sorts by the name of the peak in column 4.
echo 'Sorting...'
sort -k5,5nr -k4,4 *peaks.bed > peaks.sorted.bed

head peaks.sorted.bed > peaks4igv.bed

tail peaks.sorted.bed >> peaks4igv.bed

echo 'bedToIgv...'
bedToIgv -i peaks4igv.bed > peaks.batch
echo 'Finished.'