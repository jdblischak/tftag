#!/usr/bin/env python
# John Blischak
# 18 Oct 2012
# Prepares fasta files from ChIP-seq peaks in order to find enriched motifs
# using MEMECHIP.
# 1) Removes summits on the Y chromosome.
# 2) Adds +/- a specified number of bp around each summit.
# 3) Extracts the fasta sequence for interval from the specified genome.
################################################################################

'''
Usage:
path/memechip.py 50 path/summits.bed path/hg19.fasta.gz > path/summits_noY.fasta

where:
50 is the number of bp to increase on either side of the summit.
summits.bed is the summit of each peak called by MACS.
hg19.fasta.gz is the fasta sequence of the entire genome (does not have to be gzipped).
'''

################################################################################
import sys
import gzip
import pybedtools as bt
################################################################################

if __name__ == '__main__':
    if(len(sys.argv) != 4):
        sys.stderr.write("\nUsage:\n[username@spudhead]$ path/memechip.py 50 path/summits.bed path/hg19.fasta.gz > path/summits.fasta\n\n")
        sys.exit(1)

    # Amount to expand window on each side of summit.
    window = int(sys.argv[1])
    #window = 50
    sys.stderr.write('Window +/- ' + str(window) + ' bp.\n')

    # Bed file with summits.
    summits = sys.argv[2]
    #summits = '/mnt/lustre/home/jdblischak/tag/sequences/JUND/MACS_sub_.0001/MACS_sub_.0001_summits.bed'
    sys.stderr.write('Summit file: ' + summits + '\n')
    
    # Genome file for extracting fasta sequences.
    genome = sys.argv[3]
    #genome = '/mnt/lustre/data/share/HumanGenome/hg19/allhg19_norandom.fasta.gz'
    sys.stderr.write('Genome file: ' + genome + '\n')

    sys.stderr.write('Removing summits on the Y chromosome...\n')
    chrY = bt.BedTool('/mnt/lustre/home/jdblischak/tag/analysis/chrY.bed')
    summits_bed = bt.BedTool(summits)
    summits_noY = summits_bed.intersect(chrY, v = True)
    assert 'hg19' in genome or 'panTro3' in genome, 'Cannot determine species of genome file (needs to include hg19 or panTro3).'
    if 'hg19' in genome:
        summits_noY_slop = summits_noY.slop(b = window,
                                            g = '/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/genomes/human.hg19.genome')
    else:
        summits_noY_slop = summits_noY.slop(b = window,
                                            g = '/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/genomes/chimp.pt3.genome')

    # Opens the genome fasta file.
    if genome[-2:] == 'gz':
        genome_fasta = gzip.open(genome, 'rb')
    else:
        genome_fasta = open(genome, 'r')
    sys.stderr.write('Extracting fasta sequences...\n')
    # bt.sequence is a wrapper for fastaFromBed.
    summits_noY_slop = summits_noY_slop.sequence(fi = genome_fasta)
    sys.stdout.write(open(summits_noY_slop.seqfn).read())
    sys.stderr.write('Finished successfully.\n')
    genome_fasta.close()
    sys.exit(0)

################################################################################
