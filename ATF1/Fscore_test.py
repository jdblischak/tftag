from nose.tools import assert_equal, assert_almost_equal, assert_true, \
    assert_false, assert_raises #, assert_is_instance

from Fscore import Fscore2


def test_base():
    '''
    Base test case.
    '''
    dnase = [ ['chr1', 102, 110],
              ['chr1', 203, 211],
              ['chr2', 902, 910],
              ['chr2', 950, 960],
            ]
    chip = {'chr1':[[100, 150], [200, 230], [500, 516]],
            'chr2':[[100, 150], [200, 230], [900, 940]],
            }
    sensitivity_predict = 3. / 6.
    precision_predict = 3. / 4.
    b = 0.5
    f_predict = (1 + b**2) * precision_predict * sensitivity_predict / \
                (b**2 * precision_predict + sensitivity_predict)
    F, precision, sensitivity = Fscore2(dnase, chip, verbose = True)
    assert_equal(F, f_predict)
    assert_equal(precision, precision_predict)
    assert_equal(sensitivity, sensitivity_predict)

    
def test_chr():
    '''
    Test if peaks/sites on unshared chromsomes are
    properly counted.
    '''
    dnase = [ ['chr1', 102, 110],
              ['chr1', 203, 211],
              ['chr2', 902, 910],
              ['chr2', 950, 960],
              ['chr3', 600, 605],
            ]
    chip = {'chr1':[[100, 150], [200, 230], [500, 516]],
            'chr2':[[100, 150], [200, 230], [900, 940]],
            'chrY':[[100, 150], [200, 230], [900, 940]],
            }
    sensitivity_predict = 3. / 9.
    precision_predict = 3. / 5.
    b = 0.5
    f_predict = (1 + b**2) * precision_predict * sensitivity_predict / \
                (b**2 * precision_predict + sensitivity_predict)
    F, precision, sensitivity = Fscore2(dnase, chip, verbose = True)
    assert_equal(F, f_predict)
    assert_equal(precision, precision_predict)
    assert_equal(sensitivity, sensitivity_predict)

    
def test_assertions():
    '''
    Test if assertions are properly raised when zero sites or peaks are supplied.
    '''
    dnase = [ ['chr1', 102, 110],
              ['chr1', 203, 211],
              ['chr2', 902, 910],
              ['chr2', 950, 960],
              ['chr3', 600, 605],
            ]
    chip = {'chr1':[[100, 150], [200, 230], [500, 516]],
            'chr2':[[100, 150], [200, 230], [900, 940]],
            'chrY':[[100, 150], [200, 230], [900, 940]],
            }

    assert_raises(AssertionError, Fscore2, [], chip)
    assert_raises(AssertionError, Fscore2, dnase, {})
 
def test_partial_overlap():
    '''
    Test if partial overlaps are counted.
    '''
    dnase = [ ['chr1', 102, 110],
              ['chr1', 203, 211],
              ['chr2', 902, 910],
              ['chr2', 950, 960],
              ['chr3', 600, 605],
            ]
    chip = {'chr1':[[100, 107], [200, 230], [500, 516]],
            'chr2':[[100, 150], [200, 230], [900, 940]],
            'chrY':[[100, 150], [200, 230], [900, 940]],
            }
    sensitivity_predict = 3. / 9.
    precision_predict = 3. / 5.
    b = 0.5
    f_predict = (1 + b**2) * precision_predict * sensitivity_predict / \
                (b**2 * precision_predict + sensitivity_predict)
    F, precision, sensitivity = Fscore2(dnase, chip, verbose = True)
    assert_equal(F, f_predict)
    assert_equal(precision, precision_predict)
    assert_equal(sensitivity, sensitivity_predict)
