#!/usr/bin/env python

import getopt
import sys
import os
import gzip
import pybedtools as bt
sys.path.append('/home/jdblischak/tag/analysis/ATF1')
from Fscore import Fscore2
import numpy as np
from scipy.stats.mstats import mquantiles
import pdb

# http://snipplr.com/view/13786/

################################################################################
# Functions
################################################################################

def get_args(argv, short, long):
    d = {}
    # Set default options

    # check arguments and options
    try:
        opts, remainder = getopt.getopt(argv, short, long)
        # show usage printout if something is bad
        for opt, arg in opts:
            if opt in ("-h, --help"):
                printUsage()
                sys.exit(2)
            if opt in ("-c, --centipede"):
                assert os.path.isfile(arg), 'File %s does not exist.\n'%(arg)
                d['centipede'] = arg
                sys.stderr.write('Centipede file: %s\n'%(arg))
            elif opt in ('-m, --macs'):
                assert os.path.isfile(arg), 'File %s does not exist.\n'%(arg)                
                d['macs'] = arg
                sys.stderr.write('MACS file: %s\n'%(arg))
    except getopt.GetoptError:
        printUsage(True)
        sys.exit(2)
    except IndexError:
        printUsage(True)
        sys.exit(2)
    return d

def printUsage(error = False):
    if error:
        print "Unknown option or missing argument."
    print """
    Usage: overlap.py -c centipede.bed -m macs_peaks.xls

    -h, --help          show this help
    -c, --centipede     bed file of Centipede calls (score should be posterior)
    -m, --macs          xls output file from MACS
    """

def BedTool_to_list(bed):
    new_list = []
    for feature in bed:
        entry = [feature.chrom, int(feature.start), int(feature.end)]
        new_list.append(entry)
    return new_list


def BedTool_to_dict(bed):
    new_dict = {}
    for feature in bed:
        if feature.chrom in new_dict:
            new_dict[feature.chrom].append([int(feature.start), int(feature.end)])
        else:
            new_dict[feature.chrom] = [[int(feature.start), int(feature.end)]]
    return new_dict


################################################################################
# Script
################################################################################

if __name__ == "__main__":
    # Set arguments
    short_args = 'hc:m:'
    long_args = ['help', 'centipede=', 'macs=']
    d_args = get_args(sys.argv[1:], short_args, long_args)

    # Files will be written to directory containing MACS files
    macs_path = os.path.realpath(d_args['macs'])
    dirname, filename = os.path.split(os.path.abspath(d_args['macs']))

    # Testing
    ## d_args = {}
    ## d_args['centipede'] = 'M01600_footprints_hg19.bed'
    ## d_args['macs'] = '../../sequences/ATF1/ATF1_19238_HH/MACS_sub_0.01_5,30/MACS_sub_0.01_5,30_peaks.xls'

    # Read in Centipede footprints from a bed file and store as BedTool object
    footprints = bt.BedTool(d_args['centipede'])
    sys.stderr.write('Number of Centipede footprints: %d\n'%(footprints.count()))

    # The default bed file output by MACS uses the -10*log10 p-value as the score.
    # To get the FDR, parse the xls file and contruct a BedTool object from
    # a string.
    macs_str = ''
    i = 1
    macs_xls = open(d_args['macs'], 'r')
    for line in macs_xls:
        if line[0] == '#' or line[0] == '\n':
            continue
        cols = line.strip().split('\t')
        if cols[0] == 'chr':
            continue
        macs_str = macs_str + '%s\t%s\t%s\tMACS_peak_%d\t%s\n'%(cols[0], cols[1], cols[2], i, cols[8])
        i += 1
    macs_xls.close()
    # sys.stderr.write(macs_str[:50])
    peaks = bt.BedTool(macs_str, from_string = True)
    sys.stderr.write('Number of MACS peaks: %d\n'%(peaks.count()))

    # Set the Centipede posterior cutoffs to investigate
    posteriors = [0.99, 0.98, 0.97, 0.96, 0.95, 0.90, 0.85, 0.80, 0.75, 0.70]
    # Obtain the deciles of the MACS FDRs to use as cutoffs.
    all_fdrs = np.array([float(x.score) for x in peaks])
    fdr_deciles = mquantiles(all_fdrs, prob = [x*.1 for x in range(1,11)])
 
    # For footprints with high confidence (high posterior), how many are
    # overlapped by MACS peaks at different FDRs?
    posterior_cutoff = 0.95
    footprints.filter(lambda x: float(x.score) >= posterior_cutoff).saveas(dirname + '/footprints_subset.bed')
    footprints_cutoff = bt.BedTool(dirname + '/footprints_subset.bed')
    assert footprints_cutoff.count() > 0, 'No footprints with posterior > %.2f'%(posterior_cutoff)
    sys.stderr.write('Number of Centidpede footprints with posterior > %.2f: %d\n'%(
                     posterior_cutoff, footprints_cutoff.count()))

    peaks_overlapping_footprints = []
    peaks_per_fdr = []
    for fdr in fdr_deciles:
        peaks_fdr = peaks.filter(lambda x: float(x.score) <= fdr)
        overlap = footprints_cutoff.window(peaks_fdr, w = 100, u = True)
        peaks_overlapping_footprints.append(overlap.count())
        # Since filter returns a generator, must remake peaks_fdr to
        # count again.
        peaks_fdr = peaks.filter(lambda x: float(x.score) <= fdr)
        peaks_per_fdr.append(peaks_fdr.count())

    # For peaks with high confidence (low FDR), how many are
    # overlapped by Centipede footprints at different posteriors?
    if fdr_deciles[0] < 10.0:
        fdr_cutoff = 10.0
    else:
        fdr_cutoff = fdr_deciles[0]
    peaks.filter(lambda x: float(x.score) <= fdr_cutoff).saveas(dirname + '/peaks_subset.bed')
    peaks_cutoff = bt.BedTool(dirname + '/peaks_subset.bed')
    sys.stderr.write('Number of MACS peaks with FDR < %.2f: %d\n'%(
                     fdr_cutoff, peaks_cutoff.count()))

    footprints_overlapping_peaks = []
    footprints_per_posterior = []
    for posterior in posteriors:
        footprints_post = footprints.filter(lambda x: float(x.score) >= posterior)
        if footprints_post.count() > 0:
            footprints_post = footprints.filter(lambda x: float(x.score) >= posterior)
            overlap = footprints_post.window(peaks_cutoff, w = 100, u = True)
            footprints_overlapping_peaks.append(overlap.count())
            footprints_post = footprints.filter(lambda x: float(x.score) >= posterior)
            footprints_per_posterior.append(footprints_post.count())            
        else:
            footprints_overlapping_peaks.append(0.0)
            footprints_per_posterior.append(0.0)
            
    # Iterate through Centipede posteriors and MACS FDRs, and
    # calculate Fscores at each iteration.
    f_score = [[] for x in posteriors]
    sens = [[] for x in posteriors]
    prec = [[] for x in posteriors]
    row = 0
    for posterior in posteriors:
        sys.stderr.write('\n%.2f\n'%(posterior))
        for fdr in fdr_deciles:
            sys.stderr.write('%.2f\t'%(fdr))
            dnase = footprints.filter(lambda x: float(x.score) >= posterior)
            dnase = BedTool_to_list(dnase)
            chip = peaks.filter(lambda x: float(x.score) <= fdr)
            chip = BedTool_to_dict(chip)
            f, sensitivity, precision = Fscore2(dnase, chip)
            f_score[row].append(f)
            sens[row].append(sensitivity)
            prec[row].append(precision)
        row += 1

    # Output data
    # Peaks overlapping footprints
    peaks_handle = open(dirname + '/' + 'peaks_overlapping_footprints.txt', 'w')
    peaks_handle.write('fdr\tpeaks\tposterior\tsites\toverlap\n')
    for i in range(len(peaks_overlapping_footprints)):
        peaks_handle.write('%.2f\t%d\t%.2f\t%d\t%d\n'%(fdr_deciles[i], peaks_per_fdr[i], \
                                                     posterior_cutoff, footprints_cutoff.count(), \
                                                     peaks_overlapping_footprints[i]))
    peaks_handle.close()

    # Footprints overlapping peaks
    footprints_handle = open(dirname + '/' + 'footprints_overlapping_peaks.txt', 'w')
    footprints_handle.write('fdr\tpeaks\tposterior\tsites\toverlap\n')
    for i in range(len(footprints_overlapping_peaks)):
        footprints_handle.write('%.2f\t%d\t%.2f\t%d\t%d\n'%(fdr_cutoff, peaks_cutoff.count(), \
                                                     posteriors[i], footprints_per_posterior[i], \
                                                     footprints_overlapping_peaks[i]))
    footprints_handle.close()

    # F, sensitivity, and precision
    f_handle = open(dirname + '/' + 'f_scores.txt', 'w')
    sens_handle = open(dirname + '/' + 'sensitivity.txt', 'w')
    prec_handle = open(dirname + '/' + 'precision.txt', 'w')
    f_handle.write('posterior' + '\t' + '\t'.join(['fdr_' + str(x) for x  in fdr_deciles]) + '\n')
    sens_handle.write('posterior' + '\t' + '\t'.join(['fdr_' + str(x) for x  in fdr_deciles]) + '\n')
    prec_handle.write('posterior' +  '\t' + '\t'.join(['fdr_' + str(x) for x  in fdr_deciles]) + '\n')

    for row in range(len(posteriors)):
        f_handle.write(str(posteriors[row]) + '\t' + '\t'.join([str(x) for x  in f_score[row]]) + '\n')
        sens_handle.write(str(posteriors[row]) + '\t' + '\t'.join([str(x) for x  in sens[row]]) + '\n')
        prec_handle.write(str(posteriors[row]) + '\t' + '\t'.join([str(x) for x  in prec[row]]) + '\n')

    f_handle.close()
    sens_handle.close()
    prec_handle.close()
    os.remove(dirname + '/footprints_subset.bed')       
    os.remove(dirname + '/peaks_subset.bed')

    

    
    
