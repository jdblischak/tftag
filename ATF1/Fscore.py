import numpy as np

chromosomes = ['chr%d'%i for i in range(1,23)]
#chromosomes.append('chrX')
#chromosomes.append('chrY')

def Fscore(sites, macs_calls, b=0.5):
    """
    Parameters:

        sites: 
            list of binding sites in basic BED format (chr,start,stop)
        macs_calls: 
            dict of MACS peaks, one key-value pair for each chromosome.
            `value` in a key-value pair should be a list of peaks, each
            element of the list is a list of [left,right] coordinates.
        b:
            this parameter weighs sensitivity over precision.
            b > 1 means sensitivity is more important than precision.

    Returns:

        F:
            F-score (http://en.wikipedia.org/wiki/F1_score)
        precision:
            fraction of sites that fall in macs peaks
        sensitivity:
            fraction of macs peaks that contain atleast one site

    """
    sites_dict = dict([(chrom,[[],[]]) for chrom in chromosomes])
    [sites_dict[site[0]][0].append(int(site[1])) for site in sites]
    [sites_dict[site[0]][1].append(int(site[2])) for site in sites]

    overlap = [0,0,0,0]
    for chrom in chromosomes:
        macs_left = np.array(macs_calls[chrom])[:,0]
        macs_right = np.array(macs_calls[chrom])[:,1]
        sites_left = sites_dict[chrom][0]
        sites_right = sites_dict[chrom][1]
        # total macs peaks
        overlap[0] += macs_left.size
        # macs peaks containing atleast one centipede site
        overlap[1] += len(set([index for l,r in zip(sites_left,sites_right) \
            for index in np.logical_and(macs_left<l,macs_right>r).nonzero()[0]]))

        # centipede site within a macs peak
        overlap[2] += np.array([np.logical_and(macs_left<l,macs_right>r).any() \
            for l,r in zip(sites_left,sites_right)]).sum()
        # total centipede peaks
        overlap[3] += len(sites_left)

    sensitivity = overlap[1]/float(overlap[0])
    precision = overlap[2]/float(overlap[3])
    F = (1+b**2)*precision*sensitivity/(b**2*precision+sensitivity)

    return F, precision, sensitivity


def Fscore2(sites, macs_calls, b=0.5, verbose = False):
    """
    Parameters:

        sites: 
            list of binding sites in basic BED format (chr,start,stop)
        macs_calls: 
            dict of MACS peaks, one key-value pair for each chromosome.
            `value` in a key-value pair should be a list of peaks, each
            element of the list is a list of [left,right] coordinates.
        b:
            this parameter weighs sensitivity over precision.
            b > 1 means sensitivity is more important than precision.

    Returns:

        F:
            F-score (http://en.wikipedia.org/wiki/F1_score)
        precision:
            fraction of sites that fall in macs peaks
        sensitivity:
            fraction of macs peaks that contain atleast one site

    """
    assert len(sites) > 0, 'Must have at least one Centipede site.'
    assert len(macs_calls) > 0, 'Must have at least one MACS peak.'
    import numpy as np
    sites_chroms = set([x[0] for x in sites])
    macs_chroms = set(macs_calls.keys())
    chromosomes = sites_chroms & macs_chroms
    chromsomes = list(chromosomes)

    if verbose:
        import sys
        sys.stderr.write('chr:\n' + ' '.join(chromosomes) + '\n')
    sites_dict = dict([(chrom,[[],[]]) for chrom in sites_chroms])
    [sites_dict[site[0]][0].append(int(site[1])) for site in sites]
    [sites_dict[site[0]][1].append(int(site[2])) for site in sites]

    overlap = [0,0,0,0]
    for chrom in chromosomes:
        macs_left = np.array(macs_calls[chrom])[:,0]
        macs_right = np.array(macs_calls[chrom])[:,1]
        sites_left = sites_dict[chrom][0]
        sites_right = sites_dict[chrom][1]
        # total macs peaks
        overlap[0] += macs_left.size
        if macs_left.size != 0 and len(sites_left) != 0:
            # macs peaks containing atleast one centipede site
            overlap[1] += len(set([index for l,r in zip(sites_left,sites_right) \
               for index in np.logical_and(macs_left<l,macs_right>r).nonzero()[0]]))
            # centipede site within a macs peak
            overlap[2] += np.array([np.logical_and(macs_left<l,macs_right>r).any() \
                          for l,r in zip(sites_left,sites_right)]).sum()
        # total centipede peaks
        overlap[3] += len(sites_left)

    # Add macs peaks on chromsomes with no centipede sites
    for chrom in macs_chroms - sites_chroms:
        macs_left = np.array(macs_calls[chrom])[:,0]
        overlap[0] += macs_left.size
    # Add centipede sites on chromsomes with no macs peaks
    for chrom in sites_chroms - macs_chroms:
        sites_left = sites_dict[chrom][0]
        overlap[3] += len(sites_left)

    if verbose:
        sys.stderr.write('Total peaks: ' + str(overlap[0]) + '\n' + \
                         'Peaks with site: ' + str(overlap[1]) + '\n' + \
                         'Sites inside peaks: ' + str(overlap[2]) + '\n' + \
                         'Total sites: ' + str(overlap[3]) + '\n')
    sensitivity = overlap[1]/float(overlap[0])
    precision = overlap[2]/float(overlap[3])
    if precision == 0.0 and sensitivity == 0.0:
        F = 0.0
    else:
        F = (1+b**2)*precision*sensitivity/(b**2*precision+sensitivity)
    return F, precision, sensitivity
