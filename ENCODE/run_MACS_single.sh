#!/bin/bash

################################################################################
# John Blischak
# 02 Oct 2012
# Script to run MACS on ChIP-seq data.
# Adapted from Carolyn and Xiang's script:
# /data/share/HCR_Chipseq/scripts/qsub_MACS.sh
# For installation of MACS, see:
# /home/jdblischak/programs/install_MACS.sh
################################################################################
# Parameters
################################################################################

P_VAL=0.0001
MFOLD=10,30
LABEL=MACS_small_${P_VAL}_$MFOLD
MACS=/mnt/lustre/home/jdblischak/programs/MACS-1.4.1/bin/macs14
# Directory where sequence files are located.
DIR=/mnt/lustre/home/jdblischak/tag/sequences/JUND
STD_LOC=$DIR/$LABEL
OUTPUT_NAME=$DIR/$LABEL/$LABEL

################################################################################
# Script
################################################################################

mkdir $DIR/$LABEL
echo 'Unzipping files...'
gunzip $DIR/rep1/seq.quality.sorted.nodup.bam.gz  $DIR/rep2/seq.quality.sorted.nodup.bam.gz  $DIR/input/seq.quality.sorted.nodup.bam.gz
echo 'Merging files...'
samtools merge $DIR/seq.quality.sorted.nodup.merged.bam  $DIR/rep1/seq.quality.sorted.nodup.bam  $DIR/rep2/seq.quality.sorted.nodup.bam
samtools index  $DIR/seq.quality.sorted.nodup.merged.bam
export PYTHONPATH=/mnt/lustre/home/jdblischak/lib/python2.6/site-packages:$PYTHONPATH
echo 'Running MACS...'
$MACS -t $DIR/tmp.bam -c $DIR/input/seq.quality.sorted.nodup.bam -n $OUTPUT_NAME --pvalue=$P_VAL --to-small --mfold=$MFOLD
echo 'Rezipping files...'
gzip $DIR/rep1/seq.quality.sorted.nodup.bam $DIR/rep2/seq.quality.sorted.nodup.bam $DIR/input/seq.quality.sorted.nodup.bam

################################################################################
