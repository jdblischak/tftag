#!/bin/bash

# John Blischak
# 10 Oct 2012

# SAMPLE=/mnt/lustre/data/downloaded/encodeDCC/wgEncodeYaleChIPseq/wgEncodeYaleChIPseqRawDataRep1Gm12878Jund.fastq.gz
SAMPLE=/mnt/lustre/data/downloaded/encodeDCC/wgEncodeYaleChIPseq/wgEncodeYaleChIPseqRawDataGm12878Input.fastq.gz
DIR=/mnt/lustre/home/jdblischak/tag/sequences/JUND/input
REFGENOME=/mnt/lustre/data/share/HumanGenome/hg19/allhg19_norandom.fasta.gz
# Paths to programs     
BWA_PATH=/mnt/lustre/home/jdblischak/programs/bwa-0.5.9rc1
BEDTOOLS_PATH=/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/bin

mkdir $DIR
$BWA_PATH/bwa aln -n 1 -t 3 $REFGENOME $SAMPLE > $DIR/seq.sai
$BWA_PATH/bwa samse -n 1 $REFGENOME  $DIR/seq.sai $SAMPLE > $DIR/seq.sam
samtools view -S -h -q 10 -F 4 -b $DIR/seq.sam > $DIR/seq.quality.bam
samtools view -S -h -f 4 -b $DIR/seq.sam > $DIR/seq.unmapped.bam
rm $DIR/seq.sai
samtools sort $DIR/seq.quality.bam $DIR/seq.quality.sorted
samtools rmdup -s $DIR/seq.quality.sorted.bam $DIR/seq.quality.sorted.nodup.bam
samtools index $DIR/seq.quality.sorted.nodup.bam
$BEDTOOLS_PATH/bamToBed -i $DIR/seq.quality.sorted.nodup.bam > $DIR/seq.quality.sorted.nodup.bed
samtools view -c $DIR/seq.quality.sorted.bam > $DIR/mapped_reads_count.txt
samtools view -c $DIR/seq.quality.sorted.nodup.bam > $DIR/mapped_reads_count_nodup.txt
samtools view -c $DIR/seq.unmapped.bam > $DIR/unmapped_reads_count.txt

gzip $DIR/seq*

