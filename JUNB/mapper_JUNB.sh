#!/bin/bash

# John Blischak
# 06 Sep 2012
# Hybrid between my old mapping script (/home/jdblischak/pol2/run_bwa.sh) and
# Darren's modified version of Bryce's mapper
# (/home/sCRIPTS/RNAseq/cusanovich_misc/newmapper.sh).
# It maps with BWA, filters out reads with quality score less than 10,
# converts from SAM to BAM, and BAM to BED.

################################################################################

# Genomes
HUMAN="/mnt/lustre/data/share/HumanGenome/hg19/allhg19_norandom.fasta.gz"
CHIMP="/mnt/lustre/data/share/HCR_Chipseq/Genomes/panTro3/panTro3_nonrandom.fa"
# Paths to programs	
BWA_PATH="/mnt/lustre/home/jdblischak/programs/bwa-0.5.9rc1"				
BEDTOOLS_PATH="/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/bin"	
# Paths to working directories
DIR="/mnt/lustre/home/jdblischak/tag/sequences/JUNB"					
OUTDIR="/mnt/lustre/home/jdblischak/tag/analysis/JUNB"

################################################################################

# Each folder contains all the sequence files for one sample.
for folder in `ls -l $DIR | grep '^d' | awk '{print $9}'`
do
    cd $DIR/$folder

    # This is unneccesary in this case because there is only one file 
    # since it was not multiplexed. But this will make it easier to 
    # adapt in the future.
    for sample in `ls *sequence.txt.gz`
    do
        # Chooses the correct genome for the sample.
        if [[ $folder == *18358* ]] || [[ $folder == *18359* ]]	
        then	
            REFGENOME=$CHIMP						
            echo "Using chimp genome for $folder."					
        else							
            if [[ $folder == *19238* ]]				
            then
                REFGENOME=$HUMAN						
                echo "Using human genome for $folder."					
            else						
                echo "Cannot identify species of $folder."	
                break						
            fi							
        fi

        # Submits each mapping job individually to the queue.
	echo "\
        cd $DIR/$folder; \
        $BWA_PATH/bwa aln -n 1 -t 3 $REFGENOME $sample > seq.sai; \
	$BWA_PATH/bwa samse -n 1 $REFGENOME  seq.sai $sample > seq.sam; \
        rm seq.sai; \
        samtools view -S -h -q 10 -F 4 -b seq.sam > seq.quality.bam; \
        samtools view -S -h -f 4 -b seq.sam > seq.unmapped.bam; \
        samtools sort seq.quality.bam seq.quality.sorted; \
        samtools rmdup -s seq.quality.sorted.bam seq.quality.sorted.nodup.bam; \
        $BEDTOOLS_PATH/bamToBed -i seq.quality.sorted.nodup.bam > seq.quality.sorted.nodup.bed; \
        samtools view -c seq.quality.sorted.bam > mapped_reads_count.txt; \
        samtools view -c seq.quality.sorted.nodup.bam > mapped_reads_count_nodup.txt; \
        samtools view -c seq.unmapped.bam > unmapped_reads_count.txt; \
        " | qsub -l h_vmem=32g -o $OUTDIR -e $OUTDIR -N "map.$sample"
    done
done

###############################################################################






