#!/usr/bin/env python
# John Blischak
# 25 Sep 2012

'''
Purpose:
Summarize the most relevant MACS output for multiple samples into one file.

Use:

[username@spudling##]$ python path/summarize_MACS.py path/data_directory > path/output_file

where
/mnt/lustre/.../data_directory = the path to the directory whose subdirectories
                                 contain the MACS subdirectories.

'''

import os
import sys
import glob

################################################################################
# Script
################################################################################

if(__name__=='__main__'):
    if(len(sys.argv) != 2):
        sys.stderr.write("\nUsage:\n[username@spudling##]$ python path/summarize_MACS.py path/data_directory > path/output_file\n\n")
        sys.exit(1)

    data_dir = sys.argv[1]

    # Search through all the sample subdirectories.
    for x in os.listdir(data_dir):
        sub1 = data_dir + '/' + x
        if os.path.isdir(sub1):
            sys.stdout.write('Searching ' + sub1 + '...\n\n')
            # Find all the MACS subdirectories.
            for y in os.listdir(sub1):
                sub2 = sub1 + '/' + y
                if os.path.isdir(sub2) and y[:4] == 'MACS':
                    sys.stdout.write(sub2 + '\n')
                    err_file = open(glob.glob(sub2 + '/job*.e*')[0], 'r')
                    for line in err_file:
                        if line[0] == '#':
                            sys.stdout.write(line)
                        elif 'WARNING' in line:
                            sys.stdout.write(line)
                        elif 'total tags in treatment' in line:
                            sys.stdout.write(line)
                        elif 'tags after filtering in treatment' in line:
                            sys.stdout.write(line)
                        elif 'total tags in control' in line:
                            sys.stdout.write(line)
                        elif 'tags after filtering in control' in line:
                            sys.stdout.write(line)
                        elif 'number of paired peaks' in line:
                            sys.stdout.write(line)
                        elif 'predicted fragment length' in line:
                            sys.stdout.write(line)
                        elif 'peaks are called' in line:
                            sys.stdout.write(line)
                        elif 'swapping treat and control' in line:
                            sys.stdout.write(line)
                    sys.stdout.write('\n\n')

    sys.exit(0)
