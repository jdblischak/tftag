#!bin/bash

# John Blischak
# 12 Sep 2012

################################################################################

sample="/mnt/lustre/home/jdblischak/tag/sequences/JUNB/18358_H"
human="FALSE" # TRUE or FALSE

cd ~/tag/analysis/JUNB

# Extract human cooridinates for JUNB DNase footprints and add +/- window around 
# the PWM site. also, convert spaces to tabs, remove header, and sort.

# window=50

# zcat M00926.Table.bed.gz | awk -v OFS='\t' -v x=$window '{print $1, $2 - x + 1, $3 + x + 1, $4, \
#    $5, $6}' | sed '1d' > tmp 
# sortBed -i tmp > dnase_footprints_hg19.bed
# rm tmp

# Extract chimp cooridinates for JUNB DNase footprints and add +/- window around
# the PWM site. also, convert spaces to tabs, remove header, and sort.

# zcat M00926.Table.bed.gz | awk -v OFS='\t' -v x=$window '{print $9, $10 - x + 1, $11 + x + 1, $4, \
#    $12, $13}' | sed '1d' > tmp
# sortBed -i tmp > dnase_footprints_pan3.bed
# rm tmp 

# Count the number of reads that fall within each of these footprints.
echo "Counting hits in footprints."
if [[ $human == "TRUE" ]] 	
then	
    echo "Using human genome for ${sample:(-7)}."
    intersectBed -a dnase_footprints_hg19.bed -b $sample/seq.quality.sorted.nodup.bed -c > $sample/dnase_counts.bed
else							
    echo "Using chimp genome for ${sample:(-7)}."
    intersectBed -a dnase_footprints_pan3.bed -b $sample/seq.quality.sorted.nodup.bed -c > $sample/dnase_counts.bed
fi					
echo "It finished!"

# Get the composite posteriors for human and chimp.
#zcat M00926.Table.bed.gz | cut -f 21 > posteriors_hg19.txt
#zcat M00926.Table.bed.gz | cut -f 22 > posteriors_pan3.txt



