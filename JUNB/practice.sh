#!/bin/bash

# John Blischak
# 06 Sep 2012
# Hybrid between my old mapping script (/home/jdblischak/pol2/run_bwa.sh) and
# Darren's modified version of Bryce's mapper
# (/home/sCRIPTS/RNAseq/cusanovich_misc/newmapper.sh).
################################################################################

REFGENOME="/mnt/lustre/data/share/HCR_Chipseq/Genomes/panTro3/panTro3_nonrandom.fa"	
BWA_PATH="/mnt/lustre/home/jdblischak/programs/bwa-0.5.9rc1"				
BEDTOOLS_PATH="/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/bin"	
DIR="/mnt/lustre/home/jdblischak/tag/sequences/JUNB"					
OUTDIR="/mnt/lustre/home/jdblischak/tag/sequences/JUNB"

################################################################################

for folder in `ls -l $DIR | grep '^d' | awk '{print $9}'`
do
    cd $DIR/$folder

    for sample in `ls *sequence.txt.gz`
    do
        echo "$DIR/$folder"
        echo "$sample"
	echo "\
        cd "$DIR/$folder"; \
        touch "correct_$sample"; \
        " | qsub -l h_vmem=2g -o $OUTDIR -e $OUTDIR -N "map.$sample"
    done
done

###############################################################################

