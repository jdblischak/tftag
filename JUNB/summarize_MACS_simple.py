#!/usr/bin/env python
# John Blischak
# 25 Sep 2012

'''
Purpose:
Summarize the most relevant MACS output for multiple samples into one file.

Use:

[username@spudling##]$ python path/summarize_MACS_simple.py path/macs_error_file* > path/output_file

where
path/macs_error_file* = the search parameter for all the error files in a
                        directory (using Linux regular expressions).
'''

import os
import sys

################################################################################
# Script
################################################################################

if(__name__=='__main__'):
    if(len(sys.argv) < 2):
        sys.stderr.write("\nUsage:\n[username@spudling##]$ python path/summarize_MACS.py path/data_directory > path/output_file\n\n")
        sys.exit(1)


    for error_file in sys.argv[1:]:
        if os.path.isfile(error_file):
            handle = open(error_file, 'r')
            for line in handle:
                if line[0] == '#':
                    sys.stdout.write(line)
                elif 'WARNING' in line:
                    sys.stdout.write(line)
                elif 'total tags in treatment' in line:
                    sys.stdout.write(line)
                elif 'tags after filtering in treatment' in line:
                    sys.stdout.write(line)
                elif 'total tags in control' in line:
                    sys.stdout.write(line)
                elif 'tags after filtering in control' in line:
                    sys.stdout.write(line)
                elif 'number of paired peaks' in line:
                    sys.stdout.write(line)
                elif 'predicted fragment length' in line:
                    sys.stdout.write(line)
                elif 'peaks are called' in line:
                    sys.stdout.write(line)
                elif 'swapping treat and control' in line:
                    sys.stdout.write(line)
            sys.stdout.write('\n\n')
            handle.close()

    sys.exit(0)
