#!/bin/bash
################################################################################

# John Blischak
# 17 Sep 2012
# Script to run MACS on ChIP-seq data.
# Adapted from Carolyn and Xiang's script:
# /data/share/HCR_Chipseq/scripts/qsub_MACS.sh
# For installation of MACS, see:
# /home/jdblischak/programs/install_MACS.sh

################################################################################
# Parameters
################################################################################

P_VAL=0.01
MFOLD=5,30
LABEL=MACS_small_${P_VAL}_$MFOLD
MACS=/mnt/lustre/home/jdblischak/programs/MACS-1.4.1/bin/macs14
# Directory that has the subdirectories with sequences.
DIR=/mnt/lustre/home/jdblischak/tag/sequences/JUNB
# Directory to send STDOUT and STDERR files.
STD_LOC=/$LABEL

################################################################################
# Script
################################################################################

for SAMPLE in `ls -l $DIR | grep '^d' | awk '{print $9}'`
do
    cd $DIR/$SAMPLE
    mkdir -p $LABEL
    cd $LABEL
    INPUT_NAME=/mnt/lustre/home/jdblischak/tag/sequences/JUNB/$SAMPLE/seq.quality.sorted.nodup.bam
    OUTPUT_NAME=/mnt/lustre/home/jdblischak/tag/sequences/JUNB/$SAMPLE/$LABEL/$LABEL

    # Chooses the correct control for the sample.
    if [[ $SAMPLE == *18358* ]] || [[ $SAMPLE == *18359* ]] 
    then    
	CONTROL=/mnt/lustre/data/share/HCR_Chipseq/Mapped/Input/Input_C_Pooled36_111206.quality.sort.nodup.bam
	echo "Using $CONTROL for $SAMPLE."                                      
    else                                                    
	if [[ $SAMPLE == *19238* ]]                         
	then
	    CONTROL=/mnt/lustre/data/share/HCR_Chipseq/Mapped/Input/Input_H_Pooled36_111206.quality.sort.nodup.bam
	    echo "Using $CONTROL for $SAMPLE."                                  
	else                                                
	    echo "Cannot identify species of $SAMPLE."      
	    break                                           
	fi                                                  
    fi

    echo "\
    export PYTHONPATH=/mnt/lustre/home/jdblischak/lib/python2.6/site-packages:$PYTHONPATH; \
    $MACS -t $INPUT_NAME -c $CONTROL -n $OUTPUT_NAME --pvalue=$P_VAL --to-small --mfold=$MFOLD; \
    " | qsub -l h_vmem=32g -cwd -N job$LABEL

done
################################################################################