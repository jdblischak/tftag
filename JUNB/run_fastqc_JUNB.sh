#!/bin/bash

## John Blischak
## 06 Sep 2012
## Perform FastQC on ChIP-seq data for JUNB transfection


for folder in `ls -l /home/jdblischak/tag/sequences/JUNB/ | grep '^d' | awk '{print $9}'`
do

  echo "
  cd $folder
  mkdir fastqc
  gunzip s_[5-8]_sequence.txt.gz
  /mnt/lustre/data/tools/FastQC/fastqc --outdir fastqc s_[5-8]_sequence.txt
  gzip s_[5-8]_sequence.txt
  " | qsub -l h_vmem=4g -cwd -N qc

done
