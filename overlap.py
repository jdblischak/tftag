#!/usr/bin/env python
# John Blischak
# 23 Oct 2012
# Prepares fasta files from ChIP-seq peaks in order to find enriched motifs
# using MEMECHIP.
# 1) Intersects two bed files.
# 2) Extracts the fasta sequence of overlapping regions from the specified genome.
# 3) Removes sequences less than 8 bp (N's do not count).
################################################################################

'''
Usage:
memechip.py file1.bed file2.bed hg19.fasta.gz > intersection.fasta

where:
file1.bed and file2.bed are the files you wish to find the overlap.
hg19.fasta.gz is the fasta sequence of the entire genome (does not have to be gzipped).
'''

################################################################################
import os
import sys
import gzip
import pybedtools as bt
################################################################################

if __name__ == '__main__':
    if(len(sys.argv) != 4):
        sys.stderr.write("\nUsage:\n[username@spudhead]$ path/overlap.py file1.bed file2.bed hg19.fasta.gz > intersection.fasta\n\n")
        sys.exit(1)

    sys.stderr.write('Working directory: ' + os.getcwd() + '\n')
    file1 = sys.argv[1]
    sys.stderr.write('bed file 1: ' + file1 + '\n')
    file2 = sys.argv[2]
    sys.stderr.write('bed file 2: ' + file2 + '\n')
    genome = sys.argv[3]
    #genome = '/mnt/lustre/data/share/HumanGenome/hg19/allhg19_norandom.fasta.gz'
    #genome = '/mnt/lustre/data/share/HCR_Chipseq/Genomes/panTro3/panTro3_nonrandom.fa'
    sys.stderr.write('Genome file: ' + genome + '\n')

    sys.stderr.write('Intersecting...\n')
    bed1 = bt.BedTool(file1)
    bed2 = bt.BedTool(file2)
    intersection = bed1.intersect(bed2)
    
    if genome[-2:] == 'gz':
        genome_fasta = gzip.open(genome, 'rb')
    else:
        genome_fasta = open(genome, 'r')
    sys.stderr.write('Extracting fasta sequences...\n')
    # bt.sequence is a wrapper for fastaFromBed.
    intersection = intersection.sequence(fi = genome_fasta)
    sys.stderr.write('Removing fasta sequences < 8 bp...\n')
    for entry in open(intersection.seqfn):
        if entry[0] == '>':
            header = entry
        elif len(entry.strip('\n').replace('N', '')) >= 8: # MEMECHIP does not
                                                           # work with
                                                           # sequences < 8 bp.
                sys.stdout.write(header  + entry)
    sys.stderr.write('Finished successfully.\n')
    genome_fasta.close()
    sys.exit(0)

################################################################################
