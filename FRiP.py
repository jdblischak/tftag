# John Blischak
# 09 Oct 2012
# Calculates the fraction of reads that fall in peaks.


import sys
import pybedtools

#reads = pybedtools.BedTool('/mnt/lustre/home/jdblischak/tag/sequences/JUND/seq.quality.sorted.nodup.merged.bam')
reads = pybedtools.BedTool(sys.argv[1])
#peaks = pybedtools.BedTool('/mnt/lustre/home/jdblischak/tag/sequences/JUND/MACS_sub_.0001/MACS_sub_.0001_peaks.bed')
peaks = pybedtools.BedTool(sys.argv[2])

reads_in_peaks = reads.intersect(peaks, bed = True, u = True)

FRiP = float(reads_in_peaks.count()) / float(reads.count()) * 100

out_handle = file('FRiP.txt', 'w')
out_handle.write(str(FRiP))
out_handle.close()


