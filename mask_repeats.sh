#!/bin/bash

# John Blischak
# 05 Oct 2012
# Remove reads that map to likely unnanotated high copy number regions as
# identified in Pickrell et al. 2011.
# At least 50% of the read has to overlap the region to be removed.

################################################################################
# Parameters
################################################################################

# Directory that has the subdirectories with sequences.
DIR=/mnt/lustre/home/jdblischak/tag/sequences/JUNB
# File with regions to mask
MASK=/mnt/lustre/home/jdblischak/Masking/seq.cov1.ONHG19.bed.gz
# Bed file with sequences to be filtered
SEQS=seq.quality.sorted.nodup.bed
# Name of output file
FILTERED=seq.quality.sorted.nodup.mask.bed
# Name of job
JOB=masking
BEDTOOLS_PATH="/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/bin"

################################################################################
# Script
################################################################################

for SAMPLE in `ls -l $DIR | grep '^d' | awk '{print $9}'`
do
    cd $DIR/$SAMPLE

    echo "\
    $BEDTOOLS_PATH/intersectBed -a $SEQS  -b $MASK -v -wa -f 0.5 > $FILTERED; \
    wc -l $FILTERED > mapped_reads_count_nodup_mask.txt
    " | qsub -l h_vmem=32g -cwd -N $JOB.$SAMPLE

done
################################################################################